# Decklights

This controls the 400 led lights on the pergola of my back deck.  The features:
- 10 different modes
- 4 brightness levels
- one and off 
- controlled by a small keypad.
