#include <FastLED.h>

#include "mode.h"
#include "mode_comengo.h"

/*
 * Mode 3 : Christmas
 *
 */

CRGB    mode3_colors[] = {CRGB::Red, CRGB::Green, CRGB::White};
ModeComengoConfig mode3_config = {
        MODE3_MINIMUM_LIT,
        MODE3_MAXIMUM_LIT,

        MODE3_LOWER_LIVE_TIME,
        MODE3_UPPER_LIVE_TIME,
        MODE3_TTL_BIAS,
        MODE3_TAG_TIME_LOWER,
        MODE3_TAG_TIME_UPPER,

        MODE3_DIMBRIGHT_INTERVAL_MS,
        MODE3_FADE_STEP,

        3,
        mode3_colors
};


void mode3(Context *context) {

    if (context->sc.change_pending == LC_TRANSFER) {
        mode_comengo_init(&mode3_config, context);
    }
    mode_comengo_runloop();
}
