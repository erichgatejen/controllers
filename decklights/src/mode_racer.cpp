#include <Arduino.h>
#include <FastLED.h>

#include "mode.h"
#include "mode_racer.h"

/*
 * Racers.
 */


// ============================================================================
// = INTERNAL CONFIG

uint8_t             mode_racer_blank_index;
uint8_t             mode_racer_color_index;

Context            *mode_racer_context;
ModeRacerConfig    *mode_racer_config;
uint8_t             mode_racer_intensity;

void mode_racer_transfer() {
    turn_all_off(mode_racer_context);
    FastLED.show();
    mode_racer_blank_index = 0;
    mode_racer_color_index = 0;
    #ifdef _DEBUG_
        Serial.println("Starting mode 1.");
    #endif
}

Change mode_racer_state() {
    Change result = LC_NONE;

    switch (mode_racer_context->sc.change_pending) {
    case LC_TURN_ONOFF:
        if (!mode_racer_context->sc.is_on) {
            turn_all_off(mode_racer_context);
            #ifdef _DEBUG_
                Serial.println("Racer off.");
            #endif
        }
        break;

    case LC_INTENSITY:
        mode_racer_intensity = get_intensity_max(mode_racer_context->sc.intensity);
        break;

    case LC_TRANSFER:
        mode_racer_transfer();
        break;

    default:
        break;
    }

    return result;
}

void mode_racer_main() {
    EVERY_N_MILLISECONDS(mode_racer_config->shift_interval_ms)
    {

        for (int i = (NUM_TOTAL_LEDS)-2; i >= 0; i--) {
            mode_racer_context->move_led(i, i+1);
        }

        if (mode_racer_blank_index == 0) {
            mode_racer_context->set_led(0, (*mode_racer_config->colors)[mode_racer_color_index]);
            mode_racer_context->get_led_ptr(0)->fadeLightBy(mode_racer_intensity);

        } else {
            mode_racer_context->set_led(0, CRGB::Black);
        }

        mode_racer_color_index++;
        if (mode_racer_color_index >= mode_racer_config->num_colors) {
            mode_racer_color_index = 0;
        }

        mode_racer_blank_index++;
        if (mode_racer_blank_index >= mode_racer_config->distance) {
            mode_racer_blank_index = 0;
        }

    }

    mode_racer_context->sc.change_pending = LC_LIGHTING;
}

void mode_racer_init(ModeRacerConfig *ctx, Context *c) {
    mode_racer_config = ctx;
    mode_racer_context = c;
}

void mode_racer_runloop() {

    mode_racer_context->sc.change_pending = mode_racer_state();

    if (mode_racer_context->sc.is_on) {
        mode_racer_main();
    }
}
