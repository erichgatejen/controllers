#include <Arduino.h>

#include "config.h"
#include "context.h"
#include "defaults.h"

void init_defaults() {
    for (uint8_t i =  PIN_DEFAULT_MODE0; i <= PIN_DEFAULT_MODE9; i++) {
        pinMode(i, INPUT_PULLUP);
    }
    for (uint8_t i =  PIN_DEFAULT_INTENSITY_0; i <= PIN_DEFAULT_INTENSITY_3; i++) {
        pinMode(i, INPUT_PULLUP);
    }
}

void defaults_intensity(Context *context) {
    for (uint8_t i =  PIN_DEFAULT_INTENSITY_0; i <= PIN_DEFAULT_INTENSITY_3; i++) {
        if (digitalRead(i) == LOW) {
            context->sc.intensity = i - PIN_DEFAULT_INTENSITY_0;
            Serial.print("Default intensity set to ");
            Serial.println(context->sc.intensity);
            return;
        }
    }
    Serial.println("WARNING: These is no default intensity switched.  Picking 0");
    context->sc.intensity = INTENSITY_0;
}

void defaults_mode(Context *context) {
    for (uint8_t i =  PIN_DEFAULT_MODE0; i <= PIN_DEFAULT_MODE9; i++) {
        if (digitalRead(i) == LOW) {
            context->default_mode = i - PIN_DEFAULT_MODE0;
            Serial.print("Default mode set to ");
            Serial.println(context->default_mode);
            return;
        }
    }
    Serial.println("WARNING: These is no default mode switched.  Picking 0");
    context->default_mode = 0;
}

