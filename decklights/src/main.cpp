#include <Arduino.h>
#include <FastLED.h>

#include "key.h"
#include "config.h"
#include "context.h"
#include "mode.h"
#include "led.h"
#include "defaults.h"

Context     *context;

void setup() {

    randomSeed(analogRead(0));

    context = new Context();
    context->init();

    Serial.begin(9600);
    delay(1000);
    #ifdef _DEBUG_
        Serial.println("Starting initialization.");
    #endif

    init_defaults();
    defaults_intensity(context);
    defaults_mode(context);

    init_keys();
    init_led(context);
    init_mode(context);

    #ifdef _DEBUG_
        Serial.println("...initialization done.");
    #endif
    Serial.flush();
    delay(500);
}

void loop() {

    (*context->mode_function) (context);
    if (context->sc.change_pending != LC_NONE) {
       FastLED.show();
    }
    context->sc.change_pending = LC_NONE;
    check_key(context);
}