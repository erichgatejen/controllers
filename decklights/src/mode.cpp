#include <Arduino.h>

#include "mode.h"
#include "config.h"


// Mode map.
void (*mode_functions[NUMBER_OF_MODES]) (Context *context) = {mode0, mode1, mode2, mode3,
      mode1, mode5, mode1, mode1, mode1, mode9};

void init_mode(Context *context) {
    context->mode_function = mode_functions[context->default_mode];
}

void transition_mode(Context *context, int mode) {
    if ((mode >= 0) && (mode < NUMBER_OF_MODES)) {
        context->mode_function = mode_functions[mode];

        // Transfer workflow.
        context->sc.lit = 0;

        #ifdef _DEBUG_
                Serial.print("Transition mode #: ");
                Serial.println(mode);
        #endif

    } else {
        #ifdef _DEBUG_
            Serial.println("Warning.  Mode out of bounds.");
        #endif
    }
}

void turn_all_off(Context *context) {
    for (int index = 0; index < NUM_TOTAL_LEDS; index++) {
        context->set_led(index, CRGB::Black);
    }
    FastLED.show();
}




