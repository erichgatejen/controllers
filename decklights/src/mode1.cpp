#include <FastLED.h>

#include "mode.h"
#include "mode_comengo.h"

/*
 * Mode 1 : Stars
 */

CRGB    mode1_colors[] = {CRGB::White};
ModeComengoConfig mode1_config = {
     MODE1_MINIMUM_LIT,
     MODE1_MAXIMUM_LIT,

     MODE1_LOWER_LIVE_TIME,
     MODE1_UPPER_LIVE_TIME,
     MODE1_TTL_BIAS,
     MODE1_TAG_TIME_LOWER,
     MODE1_TAG_TIME_UPPER,

     MODE1_DIMBRIGHT_INTERVAL_MS,
     MODE1_FADE_STEP,

     1,
     mode1_colors
};

void mode1(Context *context) {

 if (context->sc.change_pending == LC_TRANSFER) {
     mode_comengo_init(&mode1_config, context);
 }
 mode_comengo_runloop();
}
