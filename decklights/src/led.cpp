#include <Arduino.h>
#include <FastLED.h>

#include "led.h"
#include "config.h"

void init_led(Context *context) {
    FastLED.addLeds<WS2811,PIN_STRING_1>(context->leds[0], NUM_LEDS_A_STRING);
    FastLED.addLeds<WS2811,PIN_STRING_2>(context->leds[1], NUM_LEDS_A_STRING);
    FastLED.addLeds<WS2811,PIN_STRING_3>(context->leds[2], NUM_LEDS_A_STRING);
    FastLED.addLeds<WS2811,PIN_STRING_4>(context->leds[3], NUM_LEDS_A_STRING);
    FastLED.setBrightness(255);
}



