#include <FastLED.h>

#include "mode.h"
#include "mode_comengo.h"

/*
 * Mode 2 : Halloween
 *
 */

CRGB    mode2_colors[] = {CRGB::DarkViolet,     // Ok
                          CRGB::LimeGreen,      // Ok
                          CRGB::DarkOrange,     // Ok
                          CRGB::DarkMagenta,    // Ok
                          CRGB::DarkRed};       // Ok
ModeComengoConfig mode2_config = {
        MODE2_MINIMUM_LIT,
        MODE2_MAXIMUM_LIT,

        MODE2_LOWER_LIVE_TIME,
        MODE2_UPPER_LIVE_TIME,
        MODE2_TTL_BIAS,
        MODE2_TAG_TIME_LOWER,
        MODE2_TAG_TIME_UPPER,

        MODE2_DIMBRIGHT_INTERVAL_MS,
        MODE2_FADE_STEP,

        5,
        mode2_colors
};


void mode2(Context *context) {

    if (context->sc.change_pending == LC_TRANSFER) {
        mode_comengo_init(&mode2_config, context);
    }
    mode_comengo_runloop();
}
