#include <Arduino.h>
#include <Keypad.h>

#include "key.h"
#include "config.h"
#include "mode.h"


char keys[KEY_ROWS][KEY_COLS] = {
        {KEY_MODE_1,KEY_MODE_2,KEY_MODE_3,KEY_INTENSITY_3},
        {KEY_MODE_4,KEY_MODE_5,KEY_MODE_6,KEY_INTENSITY_2},
        {KEY_MODE_7,KEY_MODE_8,KEY_MODE_9,KEY_INTENSITY_1},
        {KEY_ON, KEY_MODE_0, KEY_OFF, KEY_INTENSITY_0}
};

byte rowPins[KEY_ROWS] = {PIN_KEYBOARD_1, PIN_KEYBOARD_2, PIN_KEYBOARD_3, PIN_KEYBOARD_4};
byte colPins[KEY_COLS] = {PIN_KEYBOARD_5, PIN_KEYBOARD_6, PIN_KEYBOARD_7, PIN_KEYBOARD_8};

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, KEY_ROWS,
                        KEY_COLS );

void init_keys() {
    // NOOP FOR NOW
}

void check_key(Context *context) {
    char key = keypad.getKey();

    if (key) {
        #ifdef _DEBUG_
            Serial.print("Key press: ");
            Serial.println(key);
        #endif

        // Dispatch
        switch(key) {
            case KEY_MODE_1:
            case KEY_MODE_2:
            case KEY_MODE_3:
            case KEY_MODE_4:
            case KEY_MODE_5:
            case KEY_MODE_6:
            case KEY_MODE_7:
            case KEY_MODE_8:
            case KEY_MODE_9:
            case KEY_MODE_0:
                transition_mode(context, key - KEY_MODE_0);
                context->sc.change_pending = LC_TRANSFER;
                break;

            case KEY_ON:
                if (context->sc.is_on == false) {
                    context->sc.is_on = true;
                    context->sc.change_pending = LC_TURN_ONOFF;
                    #ifdef _DEBUG_
                        Serial.println("Turn on.");
                    #endif
                }
                break;

            case KEY_OFF:
                if (context->sc.is_on == true) {
                    context->sc.is_on = false;
                    context->sc.change_pending = LC_TURN_ONOFF;
                    #ifdef _DEBUG_
                        Serial.println("Turn off.");
                    #endif
                }
                break;

            case KEY_INTENSITY_3:
                context->sc.intensity = INTENSITY_3;
                context->sc.change_pending = LC_INTENSITY;
                #ifdef _DEBUG_
                    Serial.println("Intensity 3.");
                #endif
                break;

            case KEY_INTENSITY_2:
                context->sc.intensity = INTENSITY_2;
                context->sc.change_pending = LC_INTENSITY;
                #ifdef _DEBUG_
                    Serial.println("Intensity 2.");
                #endif
                break;

            case KEY_INTENSITY_1:
                context->sc.intensity = INTENSITY_1;
                context->sc.change_pending = LC_INTENSITY;
                #ifdef _DEBUG_
                    Serial.println("Intensity 1.");
                #endif
                break;

            case KEY_INTENSITY_0:
                context->sc.intensity = INTENSITY_0;
                context->sc.change_pending = LC_INTENSITY;
                #ifdef _DEBUG_
                    Serial.println("Intensity 0.");
                #endif
                break;

            default:
                #ifdef _DEBUG_
                    Serial.print("Unknown key: ");
                    Serial.println(key);
                #endif
                break;

        }
    }
}


