#include <Arduino.h>

#include "config.h"
#include "context.h"

void Context::init() {
    sc.change_pending = LC_TRANSFER;     // We are turning it on.
    sc.is_on = true;
    sc.intensity = INTENSITY_2;
    sc.debug = true;
    for (int i = 0; i < NUM_STRINGS; i++) {
        for (int ii = 0; ii < NUM_STRINGS; ii++) {
            leds[i][ii] = CRGB::Black;
        }
    }
}

#define CONTEXT_CHECK_BOUNDS(X,B,M,R) if (X >= B){ if(sc.debug){Serial.println("WARNING: M (num) out of bounds.");} return R;}

uint8_t Context::get_state(int num) {
    CONTEXT_CHECK_BOUNDS(num,NUM_TOTAL_LEDS,get_state,0)
    auto r = div(num, NUM_LEDS_A_STRING);
    return state[r.quot][r.rem];
}

uint8_t Context::set_state(int num, int s) {
    CONTEXT_CHECK_BOUNDS(num,NUM_TOTAL_LEDS,set_led,0)
    auto r = div(num, NUM_LEDS_A_STRING);
    state[r.quot][r.rem] = s;
    return s;
}

uint8_t Context::get_fade(int num) {
    CONTEXT_CHECK_BOUNDS(num,NUM_TOTAL_LEDS,get_fade,0)
    auto r = div(num, NUM_LEDS_A_STRING);
    return fade[r.quot][r.rem];
}

uint8_t Context::set_fade(int num, int s) {
    CONTEXT_CHECK_BOUNDS(num,NUM_TOTAL_LEDS,set_fade,0)
    auto r = div(num, NUM_LEDS_A_STRING);
    fade[r.quot][r.rem] = s;
    return s;
}

CRGB Context::get_led(int num) {
    CONTEXT_CHECK_BOUNDS(num,NUM_TOTAL_LEDS,get_led,CRGB::Black)
    auto r = div(num, NUM_LEDS_A_STRING);
    return leds[r.quot][r.rem];
}

CRGB Context::set_led(int num, CRGB led) {
    CONTEXT_CHECK_BOUNDS(num,NUM_TOTAL_LEDS,get_led,CRGB::Black)
    auto r = div(num, NUM_LEDS_A_STRING);
    leds[r.quot][r.rem] = led;
    return led;
}
CRGB *Context::get_led_ptr(int num) {
    CONTEXT_CHECK_BOUNDS(num,NUM_TOTAL_LEDS,get_led_ptr,nullptr);
    auto r = div(num, NUM_LEDS_A_STRING);
    return &leds[r.quot][r.rem];
}

CRGB *Context::move_led(int from, int to) {
    CONTEXT_CHECK_BOUNDS(to,NUM_TOTAL_LEDS,move_led, nullptr);
    CONTEXT_CHECK_BOUNDS(from,NUM_TOTAL_LEDS,move_led, nullptr);
    auto f = div(from, NUM_LEDS_A_STRING);
    auto t = div(to, NUM_LEDS_A_STRING);
    leds[t.quot][t.rem] = leds[f.quot][f.rem];
    return nullptr;
}


