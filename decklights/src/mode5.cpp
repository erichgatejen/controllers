#include <FastLED.h>

#include "mode.h"
#include "mode_comengo.h"

/*
 * Mode 5 : Candycorn
 *
 */

CRGB    mode5_colors[] = {CRGB::Red, CRGB::Orange, CRGB::White};
ModeComengoConfig mode5_config = {
        MODE5_MINIMUM_LIT,
        MODE5_MAXIMUM_LIT,

        MODE5_LOWER_LIVE_TIME,
        MODE5_UPPER_LIVE_TIME,
        MODE5_TTL_BIAS,
        MODE5_TAG_TIME_LOWER,
        MODE5_TAG_TIME_UPPER,

        MODE5_DIMBRIGHT_INTERVAL_MS,
        MODE5_FADE_STEP,

        3,
        mode5_colors
};


void mode5(Context *context) {

    if (context->sc.change_pending == LC_TRANSFER) {
        mode_comengo_init(&mode5_config, context);
    }
    mode_comengo_runloop();
}
