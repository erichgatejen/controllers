 #include <Arduino.h>
#include <FastLED.h>

#include "mode.h"
#include "config.h"

/*
 * Mode  0 : Stars
 */

// ============================================================================
// = TUNABLE CONFIG



// ============================================================================
// = INTERNAL CONFIG


void mode0_set_intensity(Context *context) {
    auto intensity = get_intensity_max(context->sc.intensity);
    for (int index = 0; index < NUM_TOTAL_LEDS; index++) {
        context->set_led(index, MODE0_COLOR);
        context->get_led_ptr(index)->fadeLightBy(255 - intensity);
    }
}

void mode0_check_on_off(Context *context, CRGB color) {
    if (context->sc.is_on) {
        for (int index = 0; index < NUM_TOTAL_LEDS; index++) {
            context->set_led(index, color);
        }
        mode0_set_intensity(context);
    } else {
        turn_all_off(context);
    }
}

void mode0_transfer(Context *context) {
    #ifdef _DEBUG_
         Serial.println("Starting mode 0.");
    #endif
}

Change mode0_state(Context *context) {
    switch(context->sc.change_pending) {

        case LC_TURN_ONOFF:
            if (context->sc.is_on) {
                mode0_check_on_off(context, MODE0_COLOR);
            } else {
                turn_all_off(context);
            }
            break;

        case LC_INTENSITY:
            if (context->sc.is_on) {
                mode0_set_intensity(context);
            }
            break;

        case LC_TRANSFER:
            mode0_transfer(context);
            mode0_check_on_off(context, MODE0_COLOR);
            break;

        default:
            break;
    }

    return context->sc.change_pending;
}

void mode0(Context *context) {
    context->sc.change_pending = mode0_state(context);
}
