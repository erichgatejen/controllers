#include <FastLED.h>

#include "mode.h"
#include "mode_comengo.h"

/*
 * Mode 4 : Forth of july
 *
 */

CRGB    mode4_colors[] = {CRGB::Red, CRGB::Blue, CRGB::White};
ModeComengoConfig mode4_config = {
        MODE4_MINIMUM_LIT,
        MODE4_MAXIMUM_LIT,

        MODE4_LOWER_LIVE_TIME,
        MODE4_UPPER_LIVE_TIME,
        MODE4_TTL_BIAS,
        MODE4_TAG_TIME_LOWER,
        MODE4_TAG_TIME_UPPER,

        MODE4_DIMBRIGHT_INTERVAL_MS,
        MODE4_FADE_STEP,

        3,
        mode4_colors
};


void mode4(Context *context) {

    if (context->sc.change_pending == LC_TRANSFER) {
        mode_comengo_init(&mode4_config, context);
    }
    mode_comengo_runloop();
}
