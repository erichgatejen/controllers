#include <FastLED.h>

#include "mode.h"
#include "mode_racer.h"

/*
 * Mode 9 : night racer
 *
 */

CRGB    mode9_colors[] = {CRGB::Gold, CRGB::Blue, CRGB::White};
ModeRacerConfig mode9_config = {
        MODE9_DISTANCE,
        MODE9_INTERVAL,

        3,
        mode9_colors
};


void mode9(Context *context) {

    if (context->sc.change_pending == LC_TRANSFER) {
        mode_racer_init(&mode9_config, context);
    }
    mode_racer_runloop();
}
