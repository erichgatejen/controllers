 #include <Arduino.h>
#include <FastLED.h>

#include "mode.h"
#include "mode_comengo.h"
#include "config.h"

/*
 *
 */


// ============================================================================
// = INTERNAL CONFIG
#define MODE_FADE_DROP_START   255
#define MODE_MAX_OFF_AT_ONCE   2
#define MODE_SLOT_RETRIES      8

#define MODE_TAG_FORMULA random(mode_comengo_config->tag_time_lower - ((mode_comengo_config->max_lit - mode_comengo_context->sc.lit) * 25), \
    mode_comengo_config->tag_time_upper - ((mode_comengo_config->max_lit - mode_comengo_context->sc.lit) * 98))

unsigned long mode_comengo_kill_barrier[NUM_TOTAL_LEDS];
unsigned long mode_comengo_next_tag;
uint8_t       mode_comengo_max_intensity_fade_value;

CRGB                mode_comengo_led_color[NUM_TOTAL_LEDS];
Context            *mode_comengo_context;
ModeComengoConfig  *mode_comengo_config;

void mode_comengo_transfer() {
    turn_all_off(mode_comengo_context);
    FastLED.show();
    #ifdef _DEBUG_
        Serial.println("Starting mode 1.");
    #endif
}

Change mode_comengo_state() {
    Change result = LC_NONE;

    switch (mode_comengo_context->sc.change_pending) {
    case LC_TURN_ONOFF:
        if (!mode_comengo_context->sc.is_on) {
            turn_all_off(mode_comengo_context);
            result = LC_LIGHTING;
        }
        break;

    case LC_INTENSITY:
        mode_comengo_max_intensity_fade_value = MODE_FADE_DROP_START - get_intensity_max(mode_comengo_context->sc.intensity);
        break;

    case LC_TRANSFER:
        mode_comengo_transfer();
        break;

    default:
        break;
    }

    return result;
}

void mode_comengo_set_next_tag(Context *mode_comengo_context)
{
    long tag_time = MODE_TAG_FORMULA;
    if (mode_comengo_context->sc.debug) {
        Serial.print("Next tag: ");
        Serial.println(tag_time);
    }
    mode_comengo_next_tag = MODE_TAG_FORMULA + millis();
}

void mode_comengo_tag_light(Context *mode_comengo_context) {

    int choice;

    int retries = MODE_SLOT_RETRIES;
    while (retries > 0)
    {
        choice  = random(0, NUM_TOTAL_LEDS);
        #ifdef _DEBUG_
            Serial.print("TAGGING LIGHT.  choice: ");
            Serial.println(choice);
        #endif

        if (mode_comengo_context->get_state(choice) == LS_OFF)
        {
            // Bias upper based on how many lit.
            unsigned int bias = (mode_comengo_config->max_lit - mode_comengo_context->sc.lit);
            bias = bias * mode_comengo_config->ttl_bias;
            long ttl = random(mode_comengo_config->ttl_lower, mode_comengo_config->ttl_upper);
            ttl = ttl + bias;

            if (mode_comengo_config->num_colors == 1) {
                mode_comengo_led_color[choice] = (*mode_comengo_config->colors)[0];
            } else {
                mode_comengo_led_color[choice] = (*mode_comengo_config->colors)[random(mode_comengo_config->num_colors)];
            }

            mode_comengo_context->sc.lit++;
            #ifdef _DEBUG_
                unsigned long ms  = millis();
                mode_comengo_kill_barrier[choice] = millis() + ttl;
                Serial.print(ms); Serial.print(" : NEW #"); Serial.print(choice);
                Serial.print(" . TTL= "); Serial.print(ttl);
                Serial.print("  Barrier= "); Serial.print(mode_comengo_kill_barrier[choice]);
                Serial.print("  Color= #"); Serial.print(String(mode_comengo_led_color[choice].red, HEX));
                Serial.print(String(mode_comengo_led_color[choice].green, HEX));
                Serial.print(String(mode_comengo_led_color[choice].blue, HEX));
                Serial.print(" . Total= "); Serial.println(mode_comengo_context->sc.lit);
            #else
                mode_comengo_kill_barrier[choice] = millis() + ttl;
            #endif

            mode_comengo_context->get_led_ptr(choice)->fadeLightBy(MODE_FADE_DROP_START);
            mode_comengo_context->set_fade(choice, MODE_FADE_DROP_START);
            mode_comengo_context->set_state(choice, LS_INCREASING);
            break;
        }
        else
        {
            retries--;
        }
    }
}

void mode_comengo_clear_killed(Context *mode_comengo_context)
{

    if (mode_comengo_context->sc.lit > mode_comengo_config->min_lit)
    {
        auto changed = 0;
        for (int index = 0; index < NUM_TOTAL_LEDS; index++)
        {
            if ((mode_comengo_kill_barrier[index] < millis()) && (mode_comengo_context->get_state(index) == LS_ON))
            {
                #ifdef _DEBUG_
                    Serial.print(millis());
                    Serial.print(" : KILL #= ");
                    Serial.print(index);
                    Serial.print("  Barrier= ");
                    Serial.println(mode_comengo_kill_barrier[index]);
                #endif

                mode_comengo_kill_barrier[index] = 0;
                mode_comengo_context->set_state(index, LS_DECREASING);
                mode_comengo_context->sc.lit--;
                changed++;
            }
            if (changed > MODE_MAX_OFF_AT_ONCE) break;
        }
    }

}

void mode_comengo_main() {
    int changed = 0;
    EVERY_N_MILLISECONDS(mode_comengo_config->dimbright_interval_ms)
    {
        for (int i = 0; i < NUM_TOTAL_LEDS; i++)
        {
            switch(mode_comengo_context->get_state(i))
            {
            case  LS_INCREASING:
                {
                    auto new_fade = mode_comengo_context->get_fade(i) - mode_comengo_config->fade_step;
                    mode_comengo_context->set_led(i, mode_comengo_led_color[i]);
                    if (new_fade <= mode_comengo_max_intensity_fade_value)
                    {
                        mode_comengo_context->get_led_ptr(i)->fadeLightBy(mode_comengo_max_intensity_fade_value);
                        mode_comengo_context->set_state(i, LS_ON);
                        #ifdef _DEBUG_
                            Serial.print(millis()); Serial.print(" : LIGHTING #"); Serial.print(i);
                            Serial.print("  Color= #"); Serial.print(String(mode_comengo_led_color[i].red, HEX));
                            Serial.print(String(mode_comengo_led_color[i].green, HEX));
                            Serial.println(String(mode_comengo_led_color[i].blue, HEX));
                        #endif
                    }
                    else
                    {
                        mode_comengo_context->set_fade(i, new_fade);
                        mode_comengo_context->get_led_ptr(i)->fadeLightBy(new_fade);
                    }
                }
                changed++;
                break;

            case LS_DECREASING:
                {
                    int new_fade = mode_comengo_context->get_fade(i) + mode_comengo_config->fade_step;
                    //mode_comengo_context->set_led(i, CRGB::White);
                    if (new_fade >= 255)
                    {
                        mode_comengo_context->set_led(i, CRGB::Black);
                        mode_comengo_context->set_state(i, LS_OFF);
                    }
                    else
                    {
                        mode_comengo_context->set_fade(i, new_fade);
                        mode_comengo_context->get_led_ptr(i)->fadeLightBy(new_fade);
                    }
                }
                changed++;
                break;

            default:
                break;
            }
        }

    }

    if (changed > 0) {
        mode_comengo_context->sc.change_pending = LC_LIGHTING;
    }

    if ((mode_comengo_context->sc.lit < mode_comengo_config->max_lit) && (mode_comengo_next_tag < millis()))
    {
        mode_comengo_tag_light(mode_comengo_context);
        mode_comengo_set_next_tag(mode_comengo_context);
    }

    mode_comengo_clear_killed(mode_comengo_context);
}

void mode_comengo_init(ModeComengoConfig *ctx, Context *c) {
    mode_comengo_config = ctx;
    mode_comengo_context = c;
}

void mode_comengo_runloop() {

    mode_comengo_context->sc.change_pending = mode_comengo_state();

    if (mode_comengo_context->sc.is_on) {
        while(mode_comengo_context->sc.lit < mode_comengo_config->min_lit)
        {
            mode_comengo_tag_light(mode_comengo_context);
        }
        mode_comengo_main();
    }
}
