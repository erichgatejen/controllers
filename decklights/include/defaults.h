//
// Created by Erich on 10/23/2023.
//
#ifndef DECKLIGHTS_DEFAULTS_H

#include "context.h"

// =====================================================================================
// =

void init_defaults();
void defaults_intensity(Context *context);
void defaults_mode(Context *context);

#define DECKLIGHTS_DEFAULTS_H
#endif //DECKLIGHTS_DEFAULTS_H
