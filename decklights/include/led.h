//
// Created by Erich on 10/23/2023.
//
#ifndef DECKLIGHTS_LED_H

#include "context.h"

// =====================================================================================
// =

void init_led(Context *context);

#define DECKLIGHTS_LED_H
#endif //DECKLIGHTS_LED_H
