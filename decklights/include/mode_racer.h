//
// Created by Erich on 10/23/2023.
//
#ifndef DECKLIGHTS_MODE_Racer_H

#include <Vector.h>
#include "context.h"

// =====================================================================================
// =

struct ModeRacerConfig {

    uint8_t      distance;
    uint8_t      shift_interval_ms;

    uint8_t      num_colors;
    CRGB         *colors[];
};

void mode_racer_init(ModeRacerConfig *config, Context *context);
void mode_racer_runloop();


#define DECKLIGHTS_MODE_RACER_H
#endif //DECKLIGHTS_MODE_RACER_H
