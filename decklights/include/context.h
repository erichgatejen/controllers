//
// Created by Erich on 10/23/2023.
//
#ifndef DECKLIGHTS_CONTEXT_H

#include <FastLED.h>
#include "config.h"

// =====================================================================================
// =

enum {
    INTENSITY_0 = 0,
    INTENSITY_1 = 1,
    INTENSITY_2 = 2,
    INTENSITY_3 = 3
};

#define MAX_INTENSITY INTENSITY_3

enum LEDState
{
    LS_OFF,
    LS_INCREASING,
    LS_ON,
    LS_DECREASING
};

enum Change {
    LC_NONE,
    LC_TURN_ONOFF,
    LC_INTENSITY,
    LC_TRANSFER,
    LC_LIGHTING
};

struct StateContext {
    // Overall state
    bool is_on;
    int  intensity;

    // Mode state - reset during transfer.
    int  lit;

    // Workflow state
    Change change_pending;
    bool debug;

    // Mode store
    //bool   known_on;
    //int    known_intensity;
};

struct Context {
    // Startup
    uint8_t     default_mode;

    // State
    StateContext sc;

    // LED state.
    CRGB    leds[NUM_STRINGS][NUM_LEDS_A_STRING];
    uint8_t fade[NUM_STRINGS][NUM_LEDS_A_STRING];
    uint8_t state[NUM_STRINGS][NUM_LEDS_A_STRING];

    void (*mode_function)(Context *context);

    // Helpers
    void init();
    uint8_t get_state(int num);
    uint8_t set_state(int num, int s);
    uint8_t get_fade(int num);
    uint8_t set_fade(int num, int s);
    CRGB get_led(int num);
    CRGB set_led(int num, CRGB led);
    CRGB *get_led_ptr(int num);
    CRGB *move_led(int from, int to);
};

#define DECKLIGHTS_CONTEXT_H
#endif //DECKLIGHTS_CONTEXT_H
