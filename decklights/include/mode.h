//
// Created by Erich on 10/23/2023.
//
#ifndef DECKLIGHTS_MODE_H

#include "context.h"

// =====================================================================================
// =

#define NUMBER_OF_MODES 10

void init_mode(Context *context);
void transition_mode(Context *context, int mode);
void turn_all_off(Context *context);

void mode0(Context *context);
void mode1(Context *context);
void mode2(Context *context);
void mode3(Context *context);
void mode4(Context *context);
void mode5(Context *context);

void mode9(Context *context);

#define DECKLIGHTS_MODE_H
#endif //DECKLIGHTS_MODE_H
