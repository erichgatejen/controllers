//
// Created by Erich on 10/23/2023.
//
#ifndef DECKLIGHTS_CONFIG_H

// =====================================================================================
// = TOP LEVEL VALUES

// Uncomment to get debug output to serial
#define _DEBUG_

#define NUM_STRINGS         4
#define NUM_LEDS_A_STRING   100
#define NUM_TOTAL_LEDS      NUM_STRINGS * NUM_LEDS_A_STRING


// =====================================================================================
// = INTENSITY MAX

#define INTENSITY_MAX_0     15
#define INTENSITY_MAX_1     65
#define INTENSITY_MAX_2     160
#define INTENSITY_MAX_3     255

extern uint8_t intensity_max_map[];

inline uint8_t get_intensity_max(const int intensity) { return intensity_max_map[intensity]; }

// =====================================================================================
// = MODE CONFIGURATIONS

#define MODE0_COLOR CRGB::White

#define MODE1_MINIMUM_LIT           40
#define MODE1_MAXIMUM_LIT           60
#define MODE1_LOWER_LIVE_TIME       18000
#define MODE1_UPPER_LIVE_TIME       39000
#define MODE1_TTL_BIAS              200
#define MODE1_TAG_TIME_LOWER        500
#define MODE1_TAG_TIME_UPPER        2000
#define MODE1_DIMBRIGHT_INTERVAL_MS 40
#define MODE1_FADE_STEP             10

#define MODE2_MINIMUM_LIT           MODE1_MINIMUM_LIT
#define MODE2_MAXIMUM_LIT           MODE1_MAXIMUM_LIT
#define MODE2_LOWER_LIVE_TIME       MODE1_LOWER_LIVE_TIME
#define MODE2_UPPER_LIVE_TIME       MODE1_UPPER_LIVE_TIME
#define MODE2_TTL_BIAS              MODE1_TTL_BIAS
#define MODE2_TAG_TIME_LOWER        MODE1_TAG_TIME_LOWER
#define MODE2_TAG_TIME_UPPER        MODE1_TAG_TIME_UPPER
#define MODE2_DIMBRIGHT_INTERVAL_MS MODE1_DIMBRIGHT_INTERVAL_MS
#define MODE2_FADE_STEP             MODE1_FADE_STEP

#define MODE3_MINIMUM_LIT           MODE1_MINIMUM_LIT
#define MODE3_MAXIMUM_LIT           MODE1_MAXIMUM_LIT
#define MODE3_LOWER_LIVE_TIME       MODE1_LOWER_LIVE_TIME
#define MODE3_UPPER_LIVE_TIME       MODE1_UPPER_LIVE_TIME
#define MODE3_TTL_BIAS              MODE1_TTL_BIAS
#define MODE3_TAG_TIME_LOWER        MODE1_TAG_TIME_LOWER
#define MODE3_TAG_TIME_UPPER        MODE1_TAG_TIME_UPPER
#define MODE3_DIMBRIGHT_INTERVAL_MS MODE1_DIMBRIGHT_INTERVAL_MS
#define MODE3_FADE_STEP             MODE1_FADE_STEP

#define MODE4_MINIMUM_LIT           MODE1_MINIMUM_LIT
#define MODE4_MAXIMUM_LIT           MODE1_MAXIMUM_LIT
#define MODE4_LOWER_LIVE_TIME       (MODE1_LOWER_LIVE_TIME - 6000)
#define MODE4_UPPER_LIVE_TIME       (MODE1_UPPER_LIVE_TIME - 13000)
#define MODE4_TTL_BIAS              MODE1_TTL_BIAS
#define MODE4_TAG_TIME_LOWER        MODE1_TAG_TIME_LOWER
#define MODE4_TAG_TIME_UPPER        MODE1_TAG_TIME_UPPER
#define MODE4_DIMBRIGHT_INTERVAL_MS MODE1_DIMBRIGHT_INTERVAL_MS
#define MODE4_FADE_STEP             20

#define MODE5_MINIMUM_LIT           MODE1_MINIMUM_LIT
#define MODE5_MAXIMUM_LIT           MODE1_MAXIMUM_LIT
#define MODE5_LOWER_LIVE_TIME       MODE1_LOWER_LIVE_TIME
#define MODE5_UPPER_LIVE_TIME       MODE1_UPPER_LIVE_TIME
#define MODE5_TTL_BIAS              MODE1_TTL_BIAS
#define MODE5_TAG_TIME_LOWER        MODE1_TAG_TIME_LOWER
#define MODE5_TAG_TIME_UPPER        MODE1_TAG_TIME_UPPER
#define MODE5_DIMBRIGHT_INTERVAL_MS MODE1_DIMBRIGHT_INTERVAL_MS
#define MODE5_FADE_STEP             MODE1_FADE_STEP

#define MODE9_DISTANCE  10
#define MODE9_INTERVAL  70


// =====================================================================================
// = TOP PIN ASSIGNMENTS

#define PIN_KEYBOARD_1      4
#define PIN_KEYBOARD_2      5
#define PIN_KEYBOARD_3      6
#define PIN_KEYBOARD_4      7
#define PIN_KEYBOARD_5      8
#define PIN_KEYBOARD_6      9
#define PIN_KEYBOARD_7      10
#define PIN_KEYBOARD_8      11

#define PIN_STRING_1        52
#define PIN_STRING_2        51
#define PIN_STRING_3        50
#define PIN_STRING_4        49

#define PIN_DEFAULT_INTENSITY_0 50
#define PIN_DEFAULT_INTENSITY_1 51
#define PIN_DEFAULT_INTENSITY_2 52
#define PIN_DEFAULT_INTENSITY_3 53

#define PIN_DEFAULT_MODE0   39
#define PIN_DEFAULT_MODE1   40
#define PIN_DEFAULT_MODE2   41
#define PIN_DEFAULT_MODE3   42
#define PIN_DEFAULT_MODE4   43
#define PIN_DEFAULT_MODE5   44
#define PIN_DEFAULT_MODE6   45
#define PIN_DEFAULT_MODE7   46
#define PIN_DEFAULT_MODE8   47
#define PIN_DEFAULT_MODE9   48



// =====================================================================================
// = KEY ASSIGNMENTS

/*
{'1','2','3','A'},
{'4','5','6','B'},
{'7','8','9','C'},
{'*','0','#','D'}
*/

#define KEY_MODE_1          '1'
#define KEY_MODE_2          '2'
#define KEY_MODE_3          '3'
#define KEY_MODE_4          '4'
#define KEY_MODE_5          '5'
#define KEY_MODE_6          '6'
#define KEY_MODE_7          '7'
#define KEY_MODE_8          '8'
#define KEY_MODE_9          '9'
#define KEY_MODE_0          '0'
#define KEY_ON              '*'
#define KEY_OFF             '#'
#define KEY_INTENSITY_3     'A'
#define KEY_INTENSITY_2     'B'
#define KEY_INTENSITY_1     'C'
#define KEY_INTENSITY_0     'D'




#define DECKLIGHTS_CONFIG_H
#endif //DECKLIGHTS_CONFIG_H
