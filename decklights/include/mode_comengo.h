//
// Created by Erich on 10/23/2023.
//
#ifndef DECKLIGHTS_MODE_COMENGO_H

#include <Vector.h>
#include "context.h"

// =====================================================================================
// =

struct ModeComengoConfig {
    uint16_t     min_lit;
    uint16_t     max_lit;

    int32_t      ttl_lower;
    int32_t      ttl_upper;
    uint16_t     ttl_bias;
    int32_t      tag_time_lower;
    int32_t      tag_time_upper;

    uint8_t      dimbright_interval_ms;
    uint8_t      fade_step;

    uint8_t      num_colors;
    CRGB         *colors[];
};

void mode_comengo_init(ModeComengoConfig *config, Context *context);
void mode_comengo_runloop();


#define DECKLIGHTS_MODE_COMENGO_H
#endif //DECKLIGHTS_MODE_COMENGO_H
