//
// Created by erich on 10/23/2023.
//
#ifndef DECKLIGHTS_KEY_H

#include "context.h"

const byte KEY_ROWS = 4;
const byte KEY_COLS = 4;

void init_keys();
void check_key(Context *context);




#define DECKLIGHTS_KEY_H
#endif //DECKLIGHTS_KEY_H
